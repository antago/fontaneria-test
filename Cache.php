<?php
namespace main\caching;

abstract class Cache extends CoreClass{	//base class for all caching mechanisms
	
	private $keys = [];	//property that is vital for any caching mechanism
	
	abstract public function exists($key);	//set of functions that should be always present but should be implemented for each caching mechanisms separately
	
	abstract public function add($key, $value);
	
	abstract public function get($key);
	
	abstract public function clear($key);
	
	public function clear_all(){	//functionality that already can be implemented
		if(!empty($this->keys)){
			foreach($this->keys as $key){
				$this->clear($key);
			}
		}
		return true;
	}
	
}
?>