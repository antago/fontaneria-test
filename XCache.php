<?php 
namespace main\caching;

class XCache extends Cache{	//example of cache mechanism class, which should be build upon a base caching class
	
	public function exists($key){	//all abstract functions of base class should be implemented by this class
		if(!isset($this->keys[$key])){
			return false;
		}
        return xcache_isset($key);
    }
    
    public function add($key, $value, $duration){	//implemented functions can have additional arguments
    	if(!isset($this->keys[$key])){
    		$this->keys[$key]=true;
    	}
    	xcache_set($key, $value, $duration);
    }
    
    public function get($key){
    	if($this->exists($key)){
    		return xcache_get($key);
    	}
    }
    
    public function clear($key){
    	unset($this->keys[$key]);
    	return xcache_unset($key);
    }

}
?>