<?php 
namespace main\caching;
namespace additional\mem_cashes;	//other classes added by developer

class MemCache extends Cache{ //more complex caching class
	
	public $use_memcached = false;	//additional properties used only with this caching mechanism
	public $options;
	public $username;
	public $password;
	private $servers;
	private $cache_interface;
	
	public function init(){	//this class defines one of the caching methods 
		$extension = $this->useMemcached ? 'memcached' : 'memcache';
        if (!extension_loaded($extension)) {
            throw new InvalidConfigException("MemCache requires PHP $extension extension to be loaded.");
        }
        if($use_memcached){
        	$this->init_memcached();
        }else{
        	$this->init_memcache();
        }
	}
	
	private function init_memcached(){	//init of selected caching method
		$this->cache_interface = new Memcached;
		return true;
	}
	
	private function init_memcache(){
		$this->cache_interface = new Memcache;
		return true;
	}
	
	public function exists($key){	//declared in core class method is used as an entry point for private methods
		if(!isset($this->keys[$key])){
			return false;
		}
        return $cache_interface->exists($key);
    }
    
    public function add($key, $value){
    	if(!isset($this->keys[$key])){
    		$this->keys[$key]=true;
    	}
    	return $cache_interface->add($key, $value);
    }
    
    public function get($key){
    	return $cache_interface->get($key);
    }
    
    public function clear($key){
    	unset($this->keys[$key]);
    	return $cache_interface->clear($key);
    }
    
}
?>